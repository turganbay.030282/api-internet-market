<?php

use App\Category;
use App\Product;
use App\ProductVariation;
use App\Variation;
use App\VariationCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variation_categories = [
            [
                'name' => 'Color',
                'variations' => [
                    ['name' => 'Color1'],
                    ['name' => 'Color2'],
                ]
            ],
            [
                'name' => 'Brand',
                'variations' => [
                    ['name' => 'Brand1'],
                    ['name' => 'Brand2'],
                ]
            ],
        ];

        $categories = [
            [
                'name' => 'Jackets',
                'products' => [
                    [
                        'name' => 'Jacket1',
                        'quantity' => 100,
                        'price' => 100
                    ],
                    [
                        'name' => 'Jacket2',
                        'quantity' => 100,
                        'price' => 100
                    ],
                ]
            ],
            [
                'name' => 'Jeans',
                'products' => [
                    [
                        'name' => 'Jeans1',
                        'quantity' => 100,
                        'price' => 100
                    ],
                    [
                        'name' => 'Jeans2',
                        'quantity' => 100,
                        'price' => 100
                    ]
                ],
            ]
        ];

        $variationIds = [];
        foreach ($variation_categories as $variation_category) {
            $createdVarCategory = VariationCategory::create(['name' => $variation_category['name']]);
            foreach ($variation_category['variations'] as $variation) {
                $createdVar = Variation::create(['variation_category_id' => $createdVarCategory->id, 'name' => $variation['name']]);
                $variationIds[] = $createdVar->id;
            }
        }


        $filteredVariationIds[] = array_filter($variationIds, function ($var){
            return ($var % 2 == 1);
        });
        $filteredVariationIds[] = array_filter($variationIds,  function ($var){
            return ($var % 2 == 0);
        });

        foreach ($categories as $category) {
            $createdCategory = Category::create(['name' => $category['name']]);
            foreach ($category['products'] as $key => $product) {
                $createdProduct = Product::create([
                    'category_id' => $createdCategory->id,
                    'name' => $product['name'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                ]);

                $createdProduct->productVariations()->attach($filteredVariationIds[$key]);
            }
        }
    }
}
