<?php

namespace Tests\Feature;

use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
        use RefreshDatabase;

    public function testCreate()
    {
        $response = $this->postJson('/api/categories', [
            'name' => 'Jeans'
        ]);
        $response->assertStatus(201);
    }

    public function testRequires()
    {
        $response = $this->json('POST', 'api/categories');
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'name' => ['The name field is required.'],
            ]
        ]);
    }


    public function testUpdate()
    {
        $category = Category::create([
            'name' => 'Jeans',
        ]);

        $response = $this->json('PUT', "api/categories/$category->id", [
            'name' => 'Jeans2'
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['id', 'name',]);

        $this->assertEquals('Jeans2', Category::find($category->id)->name);

    }

    public function testDelete()
    {
        $category = Category::create([
            'name' => 'Jeans',
        ]);

        $response = $this->json('DELETE', "api/categories/$category->id");
        $response->assertNoContent(204);
    }
}
