

## Installation

- `git clone git@gitlab.com:turganbay.030282/api-internet-market.git`
- `cd api-internet-market`
- `docker-compose up -d`
- `docker-compose exec app bash`
- `composer install`
- `docker-compose exec app cp .env.example .env`
- `docker-compose exec app php artisan key:generate`
- `docker-compose exec app php artisan migrate`
- `docker-compose exec app php artisan db:seed`
- `docker-compose exec app composer test`


## API Endpoints
***Accept:***  application/json

# categories

- **GET** 
/api/categories 

- **POST**
/api/categories 

params:  name 

- **GET**
/api/categories/{id}

- **PUT**
/api/categories/{id} 

params:  name 

- **DELETE**
/api/categories/{id}

- **GET**
/api/categories/{id}/products

# products

- **GET**
/api/products

- **POST**
/api/products 

params: category_id name price quantity 

- **GET**
/api/products/{id}

- **PUT**
/api/products/{id} 

params: category_id name price quantity 

- **DELETE**
/api/products/{id}

- **PUT**
/api/products/{id}/attach-variation  

params: variation_id 

- **PUT**
/api/products/{id}/detach-variation  

params: variation_id 

# orders

- **GET**
/api/orders

- **POST**
/api/orders 

params: user_id address phone amount payment_method currency paid status basket[1][quantity] 

- **GET**
/api/orders/{id}

- **PUT**
/api/orders/{id} 

params: user_id address phone amount payment_method currency paid status 

- **DELETE**
/api/orders/{id}

# payment

- **POST**
/api/payment

params: order_id amount currency card_number cvv date email phone 


# auth

- **POST** 
/api/register 

params: name email password password_confirmation 


- **POST**
/api/login 

params: email password 

- **POST**
/api/logout

headers: Authorization: Bearer `access_token`


# Cache
- **GET**

/cache-flush
