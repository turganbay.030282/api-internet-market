<?php
Route::post('payment', 'PaymentController@pay');

Route::get('categories', 'CategoryController@index');
Route::get('categories/{category}', 'CategoryController@show');
Route::get('categories/{category}/products', 'CategoryController@products');
Route::post('categories', 'CategoryController@store');
Route::put('categories/{category}', 'CategoryController@update');
Route::delete('categories/{category}', 'CategoryController@delete');

Route::get('products', 'ProductController@index');
Route::get('products/{product}', 'ProductController@show');
Route::post('products', 'ProductController@store');
Route::put('products/{product}', 'ProductController@update');
Route::delete('products/{product}', 'ProductController@delete');
Route::put('products/{product}/attach-variation', 'ProductController@attachVariation');
Route::put('products/{product}/detach-variation', 'ProductController@detachVariation');

Route::get('orders', 'OrderController@index');
Route::get('orders/{order}', 'OrderController@show');
Route::post('orders', 'OrderController@store');
Route::put('orders/{order}', 'OrderController@update');
Route::delete('orders/{order}', 'OrderController@delete');

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('users', 'UserController@index');
    Route::get('users/{user}', 'UserController@show');
    Route::post('users', 'UserController@store');
    Route::put('users/{user}', 'UserController@update');
    Route::delete('users/{user}', 'UserController@delete');

});


/*
Accept:application/json
Content-Type:application/x-www-form-urlencoded (post)

GET    /api/categories
POST   /api/categories   [name]
PUT    /api/categories/1  [name]
DELETE /api/categories/1
GET    /api/categories/1/products

GET    /api/products
POST   /api/products   ['category_id','name', 'price', 'quantity',]
PUT    /api/products/5  ['category_id','name', 'price', 'quantity',]
DELETE /api/products/5
PUT    /api/products/6/attach-variation  [variation_id]
PUT    /api/products/6/detach-variation  [variation_id]

GET    /api/orders
GET    /api/orders/1
POST   /api/orders   ['user_id' 'address','phone','amount', 'payment_method','currency','paid', 'status', basket[1][quantity]]
PUT    /api/orders/5  ['user_id' 'address','phone','amount', 'payment_method','currency','paid', 'status']
DELETE /api/orders/5

POST   /api/payment   ['order_id' 'amount' 'currency'  'card_number' 'cvv' 'date'  'email' 'phone'

 */
