<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Cache;
use Ixudra\Curl\Facades\Curl;

Route::get('/', function () {

//    $data = [
//        'intent' => 'sale',
//        'payer' => [
//            'payment_method' => 'paypal',
//        ],
//        'transactions' => [
//            [
//                'amount' => [
//                    'total' => '30.11',
//                    'currency' => 'USD',
//                    'details' => [
//                        'subtotal' => '30.00',
//                        'tax' => '0.07',
//                        'shipping' => '0.03',
//                        'handling_fee' => '1.00',
//                        'shipping_discount' => '-1.00',
//                        'insurance' => '0.01',
//                    ],
//                ],
//                'description' => 'The payment transaction description.',
//                'custom' => 'EBAY_EMS_90048630024435',
//                'invoice_number' => '48787589673',
//                'payment_options' => [
//                    'allowed_payment_method' => 'INSTANT_FUNDING_SOURCE',
//                ],
//                'soft_descriptor' => 'ECHI5786786',
//                'item_list' => [
//                    'items' => [
//                        [
//                            'name' => 'hat',
//                            'description' => 'Brown hat.',
//                            'quantity' => '5',
//                            'price' => '3',
//                            'tax' => '0.01',
//                            'sku' => '1',
//                            'currency' => 'USD',
//                        ],
//                        [
//                            'name' => 'handbag',
//                            'description' => 'Black handbag.',
//                            'quantity' => '1',
//                            'price' => '15',
//                            'tax' => '0.02',
//                            'sku' => 'product34',
//                            'currency' => 'USD',
//                        ],
//                    ],
//                    'shipping_address' => [
//                        'recipient_name' => 'Brian Robinson',
//                        'line1' => '4th Floor',
//                        'line2' => 'Unit #34',
//                        'city' => 'San Jose',
//                        'country_code' => 'US',
//                        'postal_code' => '95131',
//                        'phone' => '011862212345678',
//                        'state' => 'CA',
//                    ],
//                ],
//            ],
//        ],
//        'note_to_payer' => 'Contact us for any questions on your order.',
//        'redirect_urls' => [
//            'return_url' => 'https://example.com/return',
//            'cancel_url' => 'https://example.com/cancel',
//        ],
//    ];
//    $response = Curl::to('https://api.sandbox.paypal.com/v1/oauth2/token')
//        ->withContentType('application/json')
//        ->withOption('USERPWD', 'AY0h_0VWmLdnOPlowWabFnPpoFUuNQw8hP9UwOSR29kAA0jshpB8d8RuzzTXIOOCdVtYLp1sSpWUObZ9:EMa3KyOZ4j8tHp0u8MT5P8o7fEK9yq-Ey1yNn_FgzDBdE6HuCbiDwQQuWWDBE0TD6pK3l7JLek_d99Xa')
//        ->withData(['grant_type' => 'client_credentials'])
//        ->post();
//    $responseData = json_decode($response, true);
//    $token = $responseData['access_token'];
//
//    dump($token);
//    $response = Curl::to('https://api.sandbox.paypal.com/v1/payments/payment')
//        ->withContentType('application/json')
//        ->withHeader('Authorization: Bearer ' . $token)
//        ->withData($data)
//        ->asJson(true)
//        ->post();
//    dump($response);
//
//
//    $response = Curl::to('https://api.sandbox.paypal.com/v1/payments/payment?count=10&start_index=0&sort_by=create_time&sort_order=desc')
//        ->withContentType('application/json')
//        ->withHeader('Authorization: Bearer ' . $token)
//        ->get();
//
//    dd($response);

    return view('welcome');
});


Route::get('/cache-flush', function () {
    Cache::flush();
    return 'Cache flushed';
});
