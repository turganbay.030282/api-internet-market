<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'address',
        'phone',
        'amount',
        'payment_method',
        'currency',
        'paid',
        'status',
    ];


    public function orderProducts()
    {
        return $this->belongsToMany(
            Product::class,
            'order_products',
            'order_id',
            'product_id'
        );
    }

}
