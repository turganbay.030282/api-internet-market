<?php


namespace App\Services\Payment;


class Payment
{

    private $cards = [
        '1111222233334444' => [
            'card_number' => '1111222233334444',
            'balance' => 100
        ],

        '2222333344445555' => [
            'card_number' => '2222333344445555',
            'balance' => 1000
        ]
    ];

    public function pay($data)
    {
        $responseCode = '03';

        if (isset($this->cards[$data['card_number']])) {
            return $responseCode = ($this->cards[$data['card_number']]['balance'] >= $data['amount']) ? '00' : '13';
        }

        return $responseCode;
    }
}
