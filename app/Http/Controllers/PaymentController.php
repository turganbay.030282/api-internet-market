<?php

namespace App\Http\Controllers;

use App\Order;
use App\Services\Payment\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function pay(Request $request, Payment $payment)
    {
        $request->validate([
            'order_id' => 'required',
            'amount' => 'required',
            'currency' => 'required',
            'card_number' => 'required',
            'cvv' => 'required',
            'date' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        $result = $payment->pay($request->all());

        if ($result == '00'){
            return response()->json(['message' => 'Payment successful'], 200);
        } else if($result == '13'){
            return response()->json(['message' => 'Payment failed'], 400);
        } else {
            return response()->json(['message' => 'Invalid card'], 400);
        }
    }
}
