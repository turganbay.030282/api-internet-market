<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    public function index()
    {
        return Cache::remember('orders', 60 * 60 * 24, function () {
            return Order::with('orderProducts')->get();
        });
    }

    public function show(Order $order)
    {
        return $order->load('orderProducts');
    }

    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'phone' => 'required',
            'amount' => 'required',
            'payment_method' => 'required',
            'currency' => 'required',
            'basket' => 'required',
        ]);

        $order = Order::create($request->except('basket'));
        $order->orderProducts()->attach($request->get('basket'));
        return response()->json($order->load('orderProducts'), 201);
    }

    public function update(Request $request, Order $order)
    {
        $request->validate([
            'address' => 'required',
            'phone' => 'required',
            'amount' => 'required',
            'payment_method' => 'required',
            'currency' => 'required',
        ]);
        $order->update($request->all());
        return response()->json($order, 200);
    }

    public function delete(Order $order)
    {
        $order->delete();
        return response()->json(null, 204);
    }
}
