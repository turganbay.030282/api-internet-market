<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    public function index()
    {
        return Cache::remember('products', 60 * 60 * 24, function () {
            return Product::all();
        });
    }

    public function show(Product $product)
    {
        return $product->load('productVariations');
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);
        $product = Product::create($request->all());
        return response()->json($product, 201);
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);
        $product->update($request->all());
        return response()->json($product, 200);
    }

    public function delete(Product $product)
    {
        $product->delete();
        return response()->json(null, 204);
    }

    public function attachVariation(Request $request, Product $product)
    {
        $request->validate([
            'variation_id' => 'required',
        ]);

        $product->productVariations()->attach($request->get('variation_id'));
        return response()->json($product->load('productVariations'), 200);
    }

    public function detachVariation(Request $request, Product $product)
    {
        $request->validate([
            'variation_id' => 'required',
        ]);
        $product->productVariations()->detach($request->get('variation_id'));
        return response()->json($product->load('productVariations'), 200);
    }
}
