<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{
    public function index()
    {
        return Cache::remember('categories', 60 * 60 * 24, function () {
            return Category::with('products.productVariations')->get();
        });
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = Category::create($request->all());
        return response()->json($category, 201);
    }

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category->update($request->all());
        return response()->json($category, 200);
    }

    public function delete(Category $category)
    {
        $category->delete();
        return response()->json(null, 204);
    }

    public function products(Category $category)
    {
        return Cache::remember('category.id.'.$category->id, 60 * 60 * 24, function () use ($category) {
            return Product::with('category', 'productVariations')
                ->whereHas('category', function ($q) use ($category) {
                    $q->where('category_id', $category->id);
                })->get();
        });
    }
}
