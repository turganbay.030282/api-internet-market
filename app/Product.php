<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'category_id',
        'name',
        'price',
        'quantity',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function productVariations()
    {
        return $this->belongsToMany(
            Variation::class,
            'product_variations',
            'product_id',
            'variation_id'
        );
    }
}
